
#include "stdafx.h"
#include <math.h>
#include <time.h>

int main()
{
	clock_t start, finish;
	start=clock();
	printf("Enter a number");
	int l, i, k;
	k = 0;
	scanf("%d", &k);	
	l = 0;
	int arr[73];
	if (k > 8 || k < 1)
		return 1;
	else
	{
		for (i = 0;i <= 9 * k;i++)
			arr[i] = 0;
		for (i = 0;i < k;i++)
			l = 10 * l + 9;
		int sum;
		for (i = 0;i <= l;i++)
		{
			sum = 0;
			while (l)
			{
				sum +=l % 10;
				l /= 10;
			}
			arr[sum]++;
		}
	}
	for (i=0;i<=9*k;i++)
		printf("%d had been found %d times",i,arr[i]);
	finish=clock();
	double f;
	f=(double)(finish-start)/CLOCKS_PER_SEC;
	printf("\r %f", f);
    return 0;
}

